# Colocar aqui o nome (base) dos arquivos 
TITLE = medida

default: a4

a4:
	pdflatex ${TITLE}.tex

clean:
	rm -f ${TITLE}.aux ${TITLE}.bbl ${TITLE}.blg ${TITLE}.fot ${TITLE}.idx ${TITLE}.ilg ${TITLE}.ind ${TITLE}.log ${TITLE}.lof ${TITLE}.toc ${TITLE}.out *~

spell: 
	aspell -l PT_BR  check capa.tex
	aspell -l PT_BR  check cap1.tex
	aspell -l PT_BR  check cap2.tex
	aspell  -l PT_BR check cap3.tex
spell2: 
	aspell  -l PT_BR  check biblio.tex
	aspell  -l PT_BR  check capa.tex
	aspell  -l PT_BR  check cap1-exercicios.tex
	aspell  -l PT_BR  check cap2-exercicios.tex

