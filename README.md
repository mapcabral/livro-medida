Introdução à Teoria da Medida e Integral de Lebesgue (livro)

Pode-se gerar o livro no formato PDF (a4) com make.

AUTOR

Marco Aurélio Palumbo Cabral (professor do Instituto de Matemática da UFRJ)

DESCRIÇÃO

Um livro de Medida e Integração (integral de Lebesgue) para curso de graduação e Mestrado contendo muitos 
exercícios em cada capítulo.
Utilizado na UFRJ (Universidade Federal do Rio de Janeiro).
Os pré-requisitos são:
    (a) Teoria (elementar) dos conjuntos;
    (b) Conceitos de Análise Real: enumerabilidade, limite, supremum e noções de topologia da reta.

Fomos cuidadosos nas motivações de cada capítulo, fazendo considerações de caráter
filosófico/histórico da matéria. Para atender ao público do livro, alunos com pouca bagagem
matemática, colocamos exercícios mais concretos do que os usualmente encontrados em livros
de medida e muitos exemplos para ilustrar as definições.

SUMÁRIO

Cap. 1 Espaço com Medida (sigma-álgebras, Medida Exterior e Método de Carathéodory, Medida de Lebesgue em R)

Cap. 2 Integração (Funções Mensuráveis, Integral de Lebesgue, Teoremas de Convergência, Riemann x Lebesgue, Radon-Nikodým e Fubini)

Cap. 3 Probabilidade e Medida

SÍTIOS

Visite https://sites.google.com/matematica.ufrj.br/mapcabral/livros-e-videos para ter acesso a outros materiais relacionados a este livro.

HISTÓRIA DAS VERSÕES

V3.0 de Março de 2016: Probabilidade em Produtos Cartesianos Infinitos; Construção de Novas sigma-Álgebras e medidas.

V2.0 de Agosto de 2015: Cap. 3 foi totalmente reescrito, com a introdução de probabilidade em espaços de funções.

V0.95 de Agosto de 2013

V0.9 de 20 de Setembro de 2012: Introduzimos hyperlinks no PDF; 43 páginas.

V0.8 Primeira Edição de 5 de Janeiro de 2010 com 41 páginas.

LICENÇA

Creative Commons CC BY-NC-SA 3.0 BR, Atribuição (BY) Uso Não-Comercial (NC) CompartilhaIgual (SA) 3.0 Brasil.
Veja arquivo LICENSE para detalhes.

